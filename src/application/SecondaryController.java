package application;

import java.io.IOException;
import java.util.ArrayList;

import Dao.Dao;
import Model.Rendezveny;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;


public class SecondaryController {

	@FXML
	private Label secondaryLabel;

	@FXML
	private TableView<Rendezveny> TableViewLista;

	@FXML
	private TableColumn<Rendezveny, String> cim;
	
	@FXML
	private TableColumn<Rendezveny, String> datum;

	@FXML
	private TableColumn<Rendezveny, String> ar;

	@FXML
	private void switchToPrimary() throws IOException {
		Main.setRoot("Sample");
	}

	@FXML
	public void initialize() throws IOException {
		System.out.println("second initialised");
		Dao dao = new Dao();
		ArrayList<Rendezveny> rend = new ArrayList<>();
		rend = dao.beolvas();

		cim.setCellValueFactory(new PropertyValueFactory<>("cim"));
		datum.setCellValueFactory(new PropertyValueFactory<>("datum"));
		ar.setCellValueFactory(new PropertyValueFactory<>("ar"));
		datum.setStyle("-fx-alignment: CENTER;");
		ar.setStyle("-fx-alignment: CENTER;");
		ObservableList<Rendezveny> rendezvenyek = FXCollections.observableArrayList();
		rendezvenyek.addAll(rend);
		TableViewLista.getItems().clear();
		TableViewLista.setItems(rendezvenyek);
		System.out.println(TableViewLista.getItems().get(0));

	}

}
