package application;

import java.io.IOException;
import java.util.ArrayList;

import Dao.Dao;
import Model.Rendezveny;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;

public class SampleController {

	@FXML
	private Label labelJegyar;

	@FXML
	private Label labelFt;

	@FXML
	private Label labelIdopont;

	@FXML
	private Button btnTotal;

	@FXML
	private Button btnPrimary;

	@FXML
	private ListView<String> listView;

	@FXML
	private TextArea textArea;

	@FXML
	private void switchToSecondary() throws IOException {
		Main.setRoot("secondary");

	}

	@FXML
	public void initialize() throws InterruptedException, IOException {
		System.out.println("first initialised");
		Dao dao = new Dao();
		ArrayList<Rendezveny> rend = new ArrayList<>();
		rend = dao.beolvas();
		String osszesSzoveg = "";
		for (Rendezveny rendezveny : rend) {
			listView.getItems().add(rendezveny.getCim());
			osszesSzoveg += rendezveny.getCim() + ", " + rendezveny.getDatum() + ", " + rendezveny.getAr() + " Ft\n\r";
		}

		textArea.setText(osszesSzoveg);

//		listView.setOnMouseClicked(new EventHandler<Event>() {
//			@Override
//			public void handle(Event event) {
//				System.out.println(listView.getSelectionModel().getSelectedItem().toString());
//			}
//		});
	}

	public void refreshTotalPrice() throws IOException {
		Dao dao = new Dao();
		ArrayList<Rendezveny> rend = new ArrayList<>();
		rend = dao.beolvas();
		int totalPrice= 0;
		for (Rendezveny rendezveny : rend) {
			totalPrice+=rendezveny.getAr();
		}
		labelFt.setText(totalPrice+" Ft");

	}

	public void refreshSample() throws IOException {
		String selectedCim = "";
		if ((selectedCim = listView.getSelectionModel().getSelectedItem()) == null) {
			listView.getSelectionModel().selectFirst();
			selectedCim = listView.getSelectionModel().getSelectedItem();
		}
		;
		Dao dao = new Dao();
		ArrayList<Rendezveny> rend = new ArrayList<>();
		rend = dao.beolvas();
		Rendezveny selRend = null;
		for (Rendezveny rendezveny : rend) {
			if (rendezveny.getCim().equals(selectedCim)) {
				selRend = rendezveny;
			}
		}
		labelIdopont.setText("Idopont : " + selRend.getDatum());
		labelJegyar.setText("Jegyar : " + selRend.getAr());

	}

}
