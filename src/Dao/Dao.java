package Dao;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;



import Model.Rendezveny;

public class Dao {

	

	public Dao() {
		super();
	}

	public ArrayList<Rendezveny> beolvas() throws IOException {
		String path = "d://rendezvenyek.txt";
		ArrayList<Rendezveny> Rendezvenyek = new ArrayList<>();
		BufferedReader br = new BufferedReader(new FileReader(new File(path)));
		String sor;
		String nev;
		String datum;
		int ar;
		while ((sor = br.readLine()) != null) {
			nev = sor.split(";")[0];
			datum = sor.split(";")[1];
			ar = Integer.valueOf(sor.split(";")[2]);
			Rendezveny rendezveny = new Rendezveny(nev, datum, ar);
			Rendezvenyek.add(rendezveny);
			}
		br.close();
		return Rendezvenyek;
	}

}
