package Model;

public class Rendezveny {
	
	private String cim;
	private String datum;
	private int ar;
	
	public Rendezveny(String cim, String datum, int ar) {
		super();
		this.cim = cim;
		this.datum = datum;
		this.ar = ar;
	}

	public String getCim() {
		return cim;
	}

	public void setCim(String cim) {
		this.cim = cim;
	}

	public String getDatum() {
		return datum;
	}

	public void setDatum(String datum) {
		this.datum = datum;
	}

	public int getAr() {
		return ar;
	}

	public void setAr(int ar) {
		this.ar = ar;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Rendezveny: ");
		builder.append(cim);
		builder.append(", datum: ");
		builder.append(datum);
		builder.append(", ar: ");
		builder.append(ar);
		return builder.toString();
	}
	
	

}
